#! /usr/bin/env python3

# Acknowledgement for the animation:
# author: Jake Vanderplas
# email: vanderplas@astro.washington.edu
# website: http://jakevdp.github.com
# license: BSD


import numpy as np
import matplotlib.pyplot as plt
from matplotlib import animation

# initial condition
omega = 10*np.pi
u_0 = lambda x: np.sin(omega*x)

# exact solution
u_exact = lambda x, t, c: u_0(x-c*t)

# velocity
c = 0.1
# final time
tf = 5

# discretization
# number of space steps
M = 200
# number of time steps
N = 100
# discretized space
X = np.linspace(0, 1, M+1)
# discretized time
T = np.linspace(0, tf, N+1)

# discretized exact solution
# is stored in a (N+1)x(M+1) array
U_exact = np.zeros((N+1, M+1))
for n, t in enumerate(T):
    # line n contains the solution at time t = n dt
    U_exact[n, :] = u_exact(X, t, c)

# animation
# First set up the figure, the axis, and the plot element we want to animate
fig = plt.figure(2)
ax = plt.axes(xlim=(0, 1), ylim=(-1.1, 1.1))
line, = ax.plot([], [])

# initialization function: plot the background of each frame
def init():
    line.set_data([], [])
    return line,

# animation function.  This is called sequentially
def animate(i):
    line.set_data(X, U_exact[i,:])
    return line,

# call the animator.  blit=True means only re-draw the parts that have changed.
# careful, on a Mac blit=True is a known bug => set it to False!
anim = animation.FuncAnimation(fig, animate, init_func=init,
                               frames=N, interval=50, blit=False)

plt.show()

