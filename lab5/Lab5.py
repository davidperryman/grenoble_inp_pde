## Elliott Perryman with Ignat Sabaev
### 
print('hi Martin. running this will take probably 30 seconds')

from matplotlib.backends.backend_pdf import PdfPages

out = PdfPages('figures.pdf')

import numpy as np
import matplotlib.pyplot as plt


# function defs
def euler_explicit(u_n):
    u_n_1 = u_n.copy()
    u_n_1[1:-1] += ν*δt*(u_n[2:]-2*u_n[1:-1]+u_n[:-2])/(δx*δx)
    return u_n_1

def euler_implicit(u_n):
    A = np.zeros((len(u_n),len(u_n)))
    A[0,0] = 1
    A[-1,-1] = 1
    k = ν*δt/δx/δx
    for i in range(1,len(u_n)-1):
        A[i, i-1] = -k
        A[i, i+1] = -k
        A[i, i] = 1 + 2*k
    b = u_n.copy()
    u_n_1 = np.linalg.solve(A,b)
    return u_n_1

def crank_nicolson(u_n):
    A = np.zeros((len(u_n),len(u_n)))
    A[0,0] = 1
    A[-1,-1] = 1
    k_v = ν/δx/δx/2
    k_t = 1./δt
    for i in range(1,len(u_n)-1):
        A[i, i-1] = -k_v
        A[i, i+1] = -k_v
        A[i, i] = k_t + 2*k_v
    
    B = np.zeros((len(u_n),len(u_n)))
    B[0,0] = 1
    B[-1,-1] = 1
    for i in range(1,len(u_n)-1):
        B[i, i-1] = k_v
        B[i, i+1] = k_v
        B[i, i] = k_t - 2*k_v
    u_n_1 = (np.linalg.inv(A) @ B @ u_n)
    return u_n_1

### define consts for part 1
u0 = lambda x: np.sin(np.pi*x)
exact_solution = lambda x, t: np.exp(-1*t) * np.sin(np.pi*x)
ν = 1./(np.pi*np.pi)

M, N, T = 30, 900, 1.
t = np.linspace(0, T, N+1)
x = np.linspace(0, 1, M+1)

δt = t[1]-t[0]
δx = x[1]-x[0]

factor = np.round(ν*δt/δx/δx, 3)

U = np.empty((N+1,M+1,3))


U[0,:,0] = u0(x)
U[0,:,1] = u0(x)
U[0,:,2] = u0(x)


for i in range(1,N+1):
    U[i,:,0] = euler_explicit(U[i-1,:,0])
    U[i,:,1] = euler_implicit(U[i-1,:,1])
    U[i,:,2] = crank_nicolson(U[i-1,:,2])

print('making a bunch of plots showing different finite difference results')
plt.subplots(4,1,figsize=(7,10))
plt.subplots_adjust(hspace=0.8)
plt.suptitle('Solutions for C='+str(factor))

## INITIAL ---------------
plt.subplot(4,1,1)
plt.plot(x, exact_solution(x, 0), label='exact solution')
plt.plot(x, U[0, :, 0], label='Euler explicit scheme')
plt.plot(x, U[0, :, 1], label='Euler implicit scheme')
plt.plot(x, U[0, :, 2], label='Crank-Nicolson scheme')
plt.title('Solution at fixed t=0')
plt.ylabel("f(0, x)")
plt.xlabel("x")
plt.legend()
plt.ylim(-.1,1.1)

## ONE HALF  ---------------
plt.subplot(4,1,2)
plt.plot(x, exact_solution(x, T/2.), label='exact solution')
plt.plot(x, U[N//2, :, 0], label='Euler explicit scheme')
plt.plot(x, U[N//2, :, 1], label='Euler implicit scheme')
plt.plot(x, U[N//2, :, 2], label='Crank-Nicolson scheme')
plt.title(r'Solution at fixed $t=\frac{T}{2}$')
plt.ylabel("f(0.5, x)")
plt.xlabel("x")
plt.legend()
plt.ylim(-.1,1.1)

plt.subplot(4,1,3)
plt.plot(t, exact_solution(0.5, t), label='exact solution')
plt.plot(t, U[:, M//2, 0], label='Euler explicit scheme')
plt.plot(t, U[:, M//2, 1], label='Euler implicit scheme')
plt.plot(t, U[:, M//2, 2], label='Crank-Nicolson scheme')
plt.title('Solution at fixed x=0.5')
plt.ylabel("f(t, 0.5)")
plt.xlabel("t")
plt.legend()
plt.ylim(-.1,1.1)


## FINAL  ---------------
plt.subplot(4,1,4)
plt.plot(x, exact_solution(x, T), label='exact solution')
plt.plot(x, U[N, :, 0], label='Euler explicit scheme')
plt.plot(x, U[N, :, 1], label='Euler implicit scheme')
plt.plot(x, U[N, :, 2], label='Crank-Nicolson scheme')
plt.title('Solution at fixed t=T')
plt.ylabel("f(1, x)")
plt.xlabel("x")
plt.legend()
plt.ylim(-.1,1.1)


# In[ ]:


N = 840
T = 1
print('this will take a minute. don\'t worry about the errors, they are because for some of the parameters, the euler explicit method blows up')

for factor in [0.02, 0.1, 0.2, 0.5, 1., 100]:
    M = int(np.round((np.sqrt(N*factor/ν))))

    t = np.linspace(0, T, N+1)
    x = np.linspace(0, 1, M+1)

    δt = t[1]-t[0]
    δx = x[1]-x[0]
    factor = np.round(ν*δt/δx/δx, 2)


    U = np.empty((N+1,M+1,3))

    U[0,:,0] = u0(x)
    U[0,:,1] = u0(x)
    U[0,:,2] = u0(x)

    #U[0,0,:] = 0
    #U[0,-1,:] = 0

    for i in range(1,N+1):
        U[i,:,0] = euler_explicit(U[i-1,:,0])
        U[i,:,1] = euler_implicit(U[i-1,:,1])
        U[i,:,2] = crank_nicolson(U[i-1,:,2])

    plt.subplots(4,1,figsize=(7,10))
    plt.subplots_adjust(hspace=0.8)
    plt.suptitle('Solutions for C='+str(factor))

    ## INITIAL ---------------
    plt.subplot(4,1,1)
    plt.plot(x, exact_solution(x, 0), label='exact solution')
    plt.plot(x, U[0, :, 0], label='Euler explicit scheme')
    plt.plot(x, U[0, :, 1], label='Euler implicit scheme')
    plt.plot(x, U[0, :, 2], label='Crank-Nicolson scheme')
    plt.title('Solution at fixed t=0')
    plt.ylabel("f(0, x)")
    plt.xlabel("x")
    plt.legend()
    plt.ylim(-.1,1.1)

    ## ONE HALF  ---------------
    plt.subplot(4,1,2)
    plt.plot(x, exact_solution(x, T/2.), label='exact solution')
    plt.plot(x, U[N//2, :, 0], label='Euler explicit scheme')
    plt.plot(x, U[N//2, :, 1], label='Euler implicit scheme')
    plt.plot(x, U[N//2, :, 2], label='Crank-Nicolson scheme')
    plt.title(r'Solution at fixed $t=\frac{T}{2}$')
    plt.ylabel("f(0.5, x)")
    plt.xlabel("x")
    plt.legend()
    plt.ylim(-.1,1.1)

    plt.subplot(4,1,3)
    plt.plot(t, exact_solution(0.5, t), label='exact solution')
    plt.plot(t, U[:, M//2, 0], label='Euler explicit scheme')
    plt.plot(t, U[:, M//2, 1], label='Euler implicit scheme')
    plt.plot(t, U[:, M//2, 2], label='Crank-Nicolson scheme')
    plt.title('Solution at fixed x=0.5')
    plt.ylabel("f(t, 0.5)")
    plt.xlabel("t")
    plt.legend()
    plt.ylim(-.1,1.1)


    ## FINAL  ---------------
    plt.subplot(4,1,4)
    plt.plot(x, exact_solution(x, T), label='exact solution')
    plt.plot(x, U[N, :, 0], label='Euler explicit scheme')
    plt.plot(x, U[N, :, 1], label='Euler implicit scheme')
    plt.plot(x, U[N, :, 2], label='Crank-Nicolson scheme')
    plt.title('Solution at fixed t=T')
    plt.ylabel("f(1, x)")
    plt.xlabel("x")
    plt.legend()
    plt.ylim(-.1,1.1)

    out.savefig()
    plt.savefig('solutions_c_'+str(factor).replace('.','_')+'.svg', bbox_inches='tight')
    #plt.show()


# In[ ]:

print('computing error, this takes a few seconds, please wait..')
M_range = np.array([5, 10, 15, 20, 30, 40, 50, 75, 100, 120, 140])
err = np.empty((len(M_range), 3))
for i, M in enumerate(M_range):
    N, T = M**2, 1.
    t = np.linspace(0, T, N+1)
    x = np.linspace(0, 1, M+1)

    δt = t[1]-t[0]
    δx = x[1]-x[0]
    
    u_n = u0(x)
    for j in range(1,N+1):
        u_n = euler_explicit(u_n)
    err[i,0] = np.max(np.abs(u_n-exact_solution(x,T)))
    
    u_n = u0(x)
    for j in range(1,N+1):
        u_n = euler_implicit(u_n)
    err[i,1] = np.max(np.abs(u_n-exact_solution(x,T)))
    
    u_n = u0(x)
    for j in range(1,N+1):
        u_n = crank_nicolson(u_n)
    err[i,2] = np.max(np.abs(u_n-exact_solution(x,T)))


# In[ ]:


plt.figure(figsize=(10,7))
plt.plot(1./M_range, err, label=['Euler explicit scheme','Euler implicit scheme','Crank-Nicolson scheme'])
plt.plot([2e-2, 2e-1], [1e-5, 1e-3], label='$O(\delta x^2)$ reference')
plt.legend();
plt.loglog()
plt.ylabel("error")
plt.xlabel(r'$\delta x$')
plt.title("Error as a function of $\delta$x")
out.savefig()
plt.savefig('error_delta_x.svg', bbox_inches='tight');


# In[ ]:


plt.figure(figsize=(10,7))
plt.plot(1./M_range**2, err, label=['Euler explicit scheme','Euler implicit scheme','Crank-Nicolson scheme'])
plt.plot([2e-4, 2e-2], [1e-5, 1e-3], label='$O(\delta t)$ reference')
plt.legend();
plt.loglog()
plt.ylabel("error")
plt.xlabel(r'$\delta t$')
plt.title("Error as a function of $\delta$t")
out.savefig()
plt.savefig('error_delta_t.svg', bbox_inches='tight');


# # part 2

# In[ ]:


def alpha(x, t, k):
    pi = np.pi
    sin = np.sin
    cos = np.cos
    pik = pi*k
    return 2*np.sum([
        (sin(pik)-pik*cos(pik))/(pik**2),
        -1*((2-pik*pik)*cos(pik)+2*pik*sin(pik)-2)/(pik**3)
    ])

def truncated(x, t, N):
    pi = np.pi
    return np.sum([alpha(x, t, k)*np.exp(-k*k*pi*pi*ν*t)*np.sin(k*pi*x) for k in range(1,N+1)], 0)


u0 = lambda x: x*(1-x)

M, N, T = 10_000, 1_000_000, 0.01
t = np.linspace(0, T, N+1)
x = np.linspace(0, 1, M+1)

δt = t[1]-t[0]
δx = x[1]-x[0]


factor = np.round(ν*δt/δx/δx, 3)
factor


print('comparing finite differences with truncated series. this takes a while because the precision is high')
u_n = u0(x)
for i in range(1,N+1):
    u_n = euler_explicit(u_n)


plt.figure(figsize=(10,7))
plt.plot(x, u_n, label='"exact solution"')
for N in [1, 5, 10]:
    plt.plot(x, truncated(x, T, N), label='Truncated solution with N='+str(N))
plt.legend()
plt.title('Truncated Infinite Sum and High Precision numerical method')
plt.xlabel('x')
plt.ylabel('u')
out.savefig()
plt.savefig('truncated_comparison.svg', bbox_inches='tight')


err = []
N_range = np.arange(40)
for N in N_range:
    err.append(np.max(np.abs(u_n-truncated( x, T, N))))
err = np.array(err)


plt.figure(figsize=(10,7))
plt.plot(N_range, err)
plt.xlabel('N')
plt.title('Difference between Truncated Sum and Finite Differences')
plt.ylabel('difference with "ground truth"')
plt.yscale('log')
out.savefig()
plt.savefig('finite_difference_vs_truncated_error.svg', bbox_inches='tight')

out.close()




